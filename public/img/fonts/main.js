'use strict';

let typingTimer;                //timer identifier
let doneTypingInterval = 4000;  //time in ms ( seconds)
let myInput = document.getElementById('name');
let loading = document.getElementById('loading');
let filterDiv = document.getElementById("filterDiv");
let video_filter = document.getElementById("video_filter");
let reset = document.getElementById("reset");
let circulos = document.getElementsByClassName("circle");
let player = document.getElementById('audio');
let picturediv1 = document.getElementById('div-picture-1');
let picturediv2 = document.getElementById('div-picture-2');
let picturediv3 = document.getElementById('div-picture-3');

window.addEventListener('load', (event) => {
  document.getElementById('name').value = "";
});

picturediv1.addEventListener('mouseover', (event) => {
  document.getElementById('picture-title-1').style.visibility = "hidden";
});
picturediv1.addEventListener('mouseout', (event) => {
  document.getElementById('picture-title-1').style.visibility = "visible";
});

picturediv2.addEventListener('mouseover', (event) => {
  document.getElementById('picture-title-2').style.visibility = "hidden";
});
picturediv2.addEventListener('mouseout', (event) => {
  document.getElementById('picture-title-2').style.visibility = "visible";
});

picturediv3.addEventListener('mouseover', (event) => {
  document.getElementById('picture-title-3').style.visibility = "hidden";
});
picturediv3.addEventListener('mouseout', (event) => {
  document.getElementById('picture-title-3').style.visibility = "visible";
});



function colorChosen(){
  //on keydown, clear the countdown 
  myInput.addEventListener('keydown', function () {
    clearTimeout(typingTimer);
  });


  myInput.addEventListener('change', function() {
      clearTimeout(typingTimer);
      if (myInput.value && myInput.readOnly == false) {
          typingTimer = setTimeout(loading.style.visibility = "visible", 8000);
          typingTimer = setTimeout(doneTyping, doneTypingInterval);
      }
  });

  function doneTyping() {
      loading.style.visibility = "hidden";
      reset.style.visibility = "visible";
      var syllablesArray = cutAWordInSylables(myInput.value.split(" ")[0]);
      var record;
      if (syllablesArray[0].length == "2"){
        record = records.get(syllablesArray[0].toUpperCase());
      } else{
        record = records.get(myInput.value.slice(0,2).toUpperCase());
      }
         
      //colorear el fondo y los circulos
      document.body.style.backgroundImage = "url('./img/fonts/" + record[3] + "')";
      document.body.style.backgroundSize = "cover";
      filterDiv.style.backgroundColor = record[0];
      filterDiv.style.opacity = 0.5;
      video_filter.style.backgroundColor = record[0];
      video_filter.style.visibility = "visible";
      for (var i = 0; i < circulos.length; i++) {
        circulos[i].style.backgroundColor = record[0]; 
        circulos[i].style.opacity = 0.7;
      }
      //escribir la frase
      myInput.value += " " + record[1];
      myInput.style.color = record[0];
      myInput.readOnly = true;

      //reproducir audio
      const audio = record[2];
      player.src = "sounds/" + audio;
      player.play();
  };    
};

function resetPage(){
  player.pause();
  reset.style.visibility = "hidden";
  myInput.readOnly = false;
  myInput.style.color = "black";
  myInput.value = "";
  filterDiv.style.backgroundColor = "#F2F2F2";
  filterDiv.style.opacity = 1;
  video_filter.style.backgroundColor = false;
  video_filter.style.visibility = "hidden";
  document.body.style.backgroundImage = false;
  for (var i = 0; i < circulos.length; i++) {
    circulos[i].style.opacity = 1;
  document.getElementById("one").style.backgroundColor = "#FF0000";
  document.getElementById("two").style.backgroundColor = "#00FFFF";
  document.getElementById("three").style.backgroundColor = "#77FF00";
  document.getElementById("four").style.backgroundColor = "#7700FF";
  document.getElementById("five").style.backgroundColor = "#00FF00";
  document.getElementById("six").style.backgroundColor = "#00FF77";
  document.getElementById("seven").style.backgroundColor = "#0077FF";
  document.getElementById("eight").style.backgroundColor = "#0000FF";
  document.getElementById("nine").style.backgroundColor = "#FF00FF";
  document.getElementById("ten").style.backgroundColor = "#FF0077";
  document.getElementById("eleven").style.backgroundColor = "#FF7700";
  document.getElementById("twelve").style.backgroundColor = "#FFFF00";    
  }
};
//ANALISIS GRAMATICAL
var cutAWordInSylables = (analizedWord) => {
  var IsThereLeftToCut = true
  var splittedWord = []
  var leftToCut = analizedWord

  var cutAgaing = () => {
    var cutted = cutASyllable(leftToCut)
    splittedWord.push(cutted[0])
    leftToCut = cutted[1]
    if (cutted.length <= 1 || analizedWord.length < 2) {
      IsThereLeftToCut = false
    }
    if (IsThereLeftToCut) {
      cutAgaing()
    }
  }
  cutAgaing()

  return splittedWord
}

////WORD PROCESING FUNCTIONS
// takes a string and returns it splitted letter by letter
var aWsplittedF = (analizedWord) => {
  var analizedWordSplit = analizedWord.split('')
  return analizedWordSplit
}

// takes a string and returns an array that distinguish between consonants , open vowels and closed vowals, making ['c', 'oV', 'cV'...]
var VowelOrConsonant = (analizedWord) => {
  var wordProcesed = []
  //tests every letter of the word
  for (var i = 0; i < analizedWord.length; i++) {
    var letterRecognized = false
    var isVowel = false
    //againts every vowel
    for (var e = 0; e < vowels.length; e++) {
      //is a vowel?
      if (vowels[e] === analizedWord[i]) {
        isVowel = true
      }
    }
    //if it istn a vowel, its a consonant
    if (isVowel === false) {
      wordProcesed.push('c')
    }
    //if its a vowel, its closed?
    if (isVowel) {
      for (var d = 0; d < closedVowels.length; d++) {
        if (closedVowels[d] === analizedWord[i] && letterRecognized === false) {
          wordProcesed.push('vC')
          letterRecognized = true
        }
      }
      if (letterRecognized === false) {
        wordProcesed.push('vO')
        letterRecognized = true
      }
    }
  }
  return wordProcesed
}

//search for a string insde a string and return an index
var getIndicesOf = (searchStr, str, caseSensitive) => {
  var searchStrLen = searchStr.length
  if (searchStrLen == 0) {
    return []
  }
  var startIndex = 0,
    index,
    indices = []
  if (!caseSensitive) {
    str = str.toLowerCase()
    searchStr = searchStr.toLowerCase()
  }
  while ((index = str.indexOf(searchStr, startIndex)) > -1) {
    indices.push(index)
    startIndex = index + searchStrLen
  }
  return indices
}

//finds vowels and returns an index of them
var indexOfVowels = (aWvowelOrConsonant) => {
  var ubicationVowels = []
  for (var i = 0; i < aWvowelOrConsonant.length; i++) {
    var repetition = 0
    if (aWvowelOrConsonant[i] === 'vO' || aWvowelOrConsonant[i] === 'vC') {
      ubicationVowels.push(i)
    }
  }
  return ubicationVowels
}

//finds diptongos and returns an index of them
var findDiptongos = (aWSplitted) => {
  var diptongosIndex = []
  for (var i = 0; i < aWSplitted.length; i++) {
    //finds if there is a closed vowel
    if (aWSplitted[i] === 'i' || aWSplitted[i] === 'u') {
      for (var e = 0; e < vowels.length; e++) {
        //finds if there is a vowel before the closed vowel
        if (aWSplitted[i - 1] === vowels[e]) {
          diptongosIndex.push(i - 1)
          //finds if there is a vowel after the closed vowel
        } else if (aWSplitted[i + 1] === vowels[e]) {
          diptongosIndex.push(i)
        }
      }
    }
  }
  //eliminates duplicates caused by two closed vowels diptongos
  var uniqueArray = diptongosIndex.filter((item, pos) => {
    return diptongosIndex.indexOf(item) == pos
  })

  return uniqueArray
}

//finds hiatos and returns an index of them
var findHiatos = (aWSplitted) => {
  var hiatosIndex = []
  for (var i = 0; i < aWSplitted.length; i++) {
    //finds if there is an open vowel
    for (var e = 0; e < openVowels.length; e++) {
      if (aWSplitted[i] === openVowels[e]) {
        for (var o = 0; o < openVowels.length; o++) {
          if (aWSplitted[i + 1] === openVowels[o]) {
            hiatosIndex.push(i)
          }
        }
      }
    }
  }
  //eliminates duplicates caused by two closed vowels diptongos
  var uniqueArray = hiatosIndex.filter((item, pos) => {
    return hiatosIndex.indexOf(item) == pos
  })

  return uniqueArray
}

//finds rr or ll and returns an index of them
var findDobleLetters = (aWSplitted) => {
  var dobleLettersIndex = []
  for (var i = 0; i < aWSplitted.length; i++) {
    //finds if there a possible doble letter

    for (var e = 0; e < possibleDobleLetters.length; e++) {
      if (aWSplitted[i] === possibleDobleLetters[e]) {
        if (aWSplitted[i] === aWSplitted[i + 1]) {
          dobleLettersIndex.push(i)
        }
      }
    }
  }

  //eliminates duplicates caused by two closed vowels diptongos
  var uniqueArray = dobleLettersIndex.filter((item, pos) => {
    return dobleLettersIndex.indexOf(item) == pos
  })
  return uniqueArray
}

//finds an array of strings and returns an index of them
var getIndicesOfThese = (findThese, text) => {
  var arr = []
  for (var i = 0; i < findThese.length; i++) {
    findThese[i]
    arr.push(getIndicesOf(findThese[i], text))
  }
  var merged = [].concat.apply([], arr)
  var mergedNsorted = merged.sort((a, b) => {
    return a - b
  })
  var uniqueArray = mergedNsorted.filter((item, pos) => {
    return mergedNsorted.indexOf(item) == pos
  })
  return uniqueArray
}

//finds unsplittables strings and returns an index of them
var findUnsplittables = (analizedWord) => {
  return getIndicesOfThese(unsplittables, analizedWord)
}

//returns a full word analysis based in the info returned by functions above
var aWanalysis = (analizedWord) => {
  var analizedWordObj = {
    aWoriginal: analizedWord,
    analizedWord: analizedWord.toLowerCase(),
    aWSplitted: aWsplittedF(analizedWord.toLowerCase()),
    aWvowelOrConsonant: VowelOrConsonant(analizedWord.toLowerCase()),
    aWindexOfVowels: indexOfVowels(
      VowelOrConsonant(analizedWord.toLowerCase())
    ),
    indexOfDiptongos: findDiptongos(aWsplittedF(analizedWord.toLowerCase())),
    indexOfHiatos: findHiatos(aWsplittedF(analizedWord.toLowerCase())),
    indexOfdobleLetters: findDobleLetters(
      aWsplittedF(analizedWord.toLowerCase())
    ),
    indexOfunsplittables: findUnsplittables(analizedWord),
    aWTotalySplitted: false,
  }
  return analizedWordObj
}

//recives a string and returns an array, with the ['first Sylable', 'the rest of the string, left to beeing cut']
var cutASyllable = (analizedWord) => {
  var firstVowelIndex = aWanalysis(analizedWord).aWindexOfVowels[0]
  var firstHiatoIndex = aWanalysis(analizedWord).indexOfHiatos[0]
  var firstDiptongoIndex = aWanalysis(analizedWord).indexOfDiptongos[0]
  var firstUnsplittableIndex = aWanalysis(analizedWord).indexOfunsplittables[0]
  var secondVowelIndex = aWanalysis(analizedWord).aWindexOfVowels[1]
  var thirdVowelIndex = aWanalysis(analizedWord).aWindexOfVowels[2]
  var firstRepeatedLetterIndex = aWanalysis(analizedWord).indexOfdobleLetters[0]
  var consonantsBetweenVowels =
    aWanalysis(analizedWord).aWindexOfVowels[1] -
    aWanalysis(analizedWord).aWindexOfVowels[0]
  var wordBeingCut = null
  var firstSyllable = null

  var cutFirstSyllableHere = (whereToCut) => {
    firstSyllable = analizedWord.substring(0, whereToCut)
    wordBeingCut = analizedWord.substring(whereToCut)
  }

  //////// HERE IS WHERE THE RULES OF SPLITTING A WORD APPLIES!!
  // if its there are no more than three letters to cut, 'cut' it in the end
  if (analizedWord.length < 2) {
    cutFirstSyllableHere(analizedWord.length)
    // if its there an hiato, cut the syllable between bowels
  } else if (firstVowelIndex === firstHiatoIndex) {
    cutFirstSyllableHere(firstHiatoIndex + 1)
    //else if its there a diptongo, cut the syllable one before the third vowel
  } else if (firstVowelIndex === firstDiptongoIndex) {
    //it theres a diptongo an then repeated letters, cut after them
    if (firstVowelIndex + 2 === firstRepeatedLetterIndex) {
      cutFirstSyllableHere(thirdVowelIndex - 2)
    } else {
      //if not, cut one letter after the third vowel
      cutFirstSyllableHere(thirdVowelIndex - 1)
    }
    //else if, there is a repeated letter, cut the syllable two letter before the second vowel
  } else if (secondVowelIndex - 2 === firstRepeatedLetterIndex) {
    cutFirstSyllableHere(secondVowelIndex - 2)
    //else if, there are four consonants between the first vowel and the second, split it after the second consonant
  } else if (secondVowelIndex - 5 === firstVowelIndex) {
    cutFirstSyllableHere(secondVowelIndex - 2)
    //else if, there are three consonants between the first vowel and the second,
  } else if (secondVowelIndex - 4 === firstVowelIndex) {
    //if there is an unsplittable string right after the vowel
    if (firstUnsplittableIndex === firstVowelIndex + 1) {
      //split it after the unsplittable. The unsplittables have two characters
      cutFirstSyllableHere(firstVowelIndex + 3)
    }
    //if there is an unsplittable string a character after the vowel
    else if (firstUnsplittableIndex === firstVowelIndex + 2) {
      //split it before unsplittable.
      cutFirstSyllableHere(firstVowelIndex + 2)
    } else {
      //if none of the above, split it right after the three consonants.
      cutFirstSyllableHere(firstVowelIndex + 3)
    }
    //else if, there are two consonants between the first vowel and the second,
  } else if (secondVowelIndex - 3 === firstVowelIndex) {
    //is there an unsplittable there?
    if (firstUnsplittableIndex === firstVowelIndex + 1) {
      //split it after the vowel
      cutFirstSyllableHere(firstVowelIndex + 1)
    } else {
      //split the two consonants in halves
      cutFirstSyllableHere(firstVowelIndex + 2)
    }
    //if none of the avobe, (there is one consonant between two vowels)
  } else {
    //split it one character before the second vowel
    cutFirstSyllableHere(secondVowelIndex - 1)
  }
  var wordProcess;
  if (firstSyllable === '') {
    wordProcess = [wordBeingCut]
  } else if (firstSyllable === '') {
    wordProcess = [wordBeingCut]
  } else {
    wordProcess = [firstSyllable, wordBeingCut]
  }
  return wordProcess
}


//CODIGO DE LA CAMARA
// Constantes
const videoFrame = document.getElementById('video_frame');
const errorMsgElement = document.querySelector('span#errorMsg');

// Tamaño del video
const constraints = {
  video: {
    width: 720, height: 405
  }
};

// En caso de que el acceso sea correcto, cargamos la camarita
async function handleSuccess(stream) {
  window.stream = stream;
  videoFrame.srcObject = stream;
}

// Comprobar acceso
async function init() {
  try {
    const stream = await navigator.mediaDevices.getUserMedia(constraints);
    handleSuccess(stream);
  } catch (e) {
    errorMsgElement.innerHTML = `navigator.getUserMedia error:${e.toString()}`;
  }
}

// Iniciamos JS
colorChosen();
init();

/////////////////////////////////////////////////////////////////////////////////////////////
//some constants realted to spanish language
var AlphabetEs = [
  'a',
  'b',
  'c',
  'd',
  'e',
  'f',
  'g',
  'h',
  'i',
  'j',
  'k',
  'l',
  'm',
  'n',
  'ñ',
  'o',
  'p',
  'q',
  'r',
  's',
  't',
  'u',
  'v',
  'w',
  'x',
  'y',
  'z',
]
var vowels = ['a', 'e', 'i', 'o', 'u', 'á', 'é', 'í', 'ó', 'ú']
var vowelsTilde = ['á', 'é', 'í', 'ó', 'ú']
var openVowels = ['a', 'e', 'o', 'á', 'é', 'í', 'ó', 'ú']
var closedVowels = ['u', 'i', 'ü']
var consonants = [
  'b',
  'c',
  'd',
  'f',
  'g',
  'h',
  'j',
  'k',
  'l',
  'm',
  'n',
  'ñ',
  'p',
  'q',
  'r',
  's',
  't',
  'v',
  'w',
  'x',
  'y',
  'z',
]
var possibleDobleLetters = ['r', 'l', 't']
var unsplittables = [
  'br',
  'cr',
  'dr',
  'gr',
  'fr',
  'kr',
  'tr',
  'bl',
  'cl',
  'gl',
  'fl',
  'kl',
  'pl',
  'gü',
  'ch',
]

//TODOS LOS REGISTROS
let records= new Map();
records.set("AB", ["#FF0000", "dulce como la padrera", "pajaro_mirlo.mp3", "pradera.jpg"]);
records.set("BA", ["#b45bb4", "amargo como el mes de abril", "lluvia.mp3", "imagen_10.jpg"]);
records.set("CA", ["#ff6a06", "fresca y dulce como los oboes", "arboles.mp3", "arboles.png"]);
records.set("AC", ["#ff8606", "tienes sabor a vainilla", "cuchara.mp3", "imagen_2.jpg"]);
records.set("DA", ["#c206b4", "tu sabor es como el... umami", "profundo.mp3", "imagen_3.png"]);
records.set("AD", ["#b5005f", "eres como un viernes color magenta", "pajaro_mirlo.mp3", "imagen_7.png"]);
records.set("AF", ["#b55f00", "hueles a calma, a manzanilla", "silbido-tetera.mp3", "imagen_2.jpg"]);
records.set("FA", ["#7cb415", "tu aroma es un picor dulce", "flauta.mp3", "hoja.jpeg"]);
records.set("GA", ["#9200b4", "me suenas a la nota  Si...", "SI.wav", "textura-3.png"]);
records.set("AG", ["#bb00b4", "tienes sabor a dulce", "naturaleza-dos.mp3", "miel.jpeg"]);
records.set("AH", ["#e20000", "dulce como un colifror", "risas-ninas-mp3", "miel.jpeg"]);
records.set("HA", ["#e20000", "tienes aroma a maltas tostadas ", "hojas-secas.mp3", "tierra.jpeg"]);
records.set("JA", ["#e2b481", "caramelo amargo", "sonido-instrumento.mp3", "miel.jpeg"]);
records.set("AJ", ["#ffdeb6", "sabes a los enigmas de la vida", "Timbre.mp3", "imagen_16.png"]);
records.set("KA", ["#e5b481", "amargo y dulce como un ámbar", "efecto-exprimir.mp3", "textura.png"]);
records.set("AK", ["#e5a66e", "eres fuerte y amarga como un almizcle", "Timbre.mp3", "sonidoagudo.png"]);
records.set("AL", ["#ffde81", "dulce como la primera mañana de enero", "Timbre.mp3", "calido.jpg"]);
records.set("LA", ["#ffb400", "me suenas a la nota  Re...", "RE.wav", "textura-notas.png"]);
records.set("MA", ["#dd5f85", "froot loops con leche ", "slurp.mp3", "cereal.png"]);
records.set("AM", ["#b886a3", "tienes sabor a la letra M", "boing-rebote.mp3", "textura.png"]);
records.set("AÑ", ["#3a1a1a", "hueles a galletas recien horneadas", "sonido-de-horno.mp3", "textura.png"]);
records.set("ÑA", ["#b94f4f", "un rojo que huele ligeramente a Ron", "aspero.mp3", "textura-negra.jpg"]);
records.set("PA", ["#31736f", "hueles a la lluvia del mes de junio", "lluvia.mp3", "imagen_10.jpg"]);
records.set("AP", ["#747471", "tu nombre huele a caucho", "tapa.mp3", "caucho.jpg"]);
records.set("AQ", ["#9e548c", "- una paleta de colita ", "peces-mar.mp3", "agua.jpeg"]);
records.set("RA", ["#c80000", "sabes al triunfo, color rojo", "ruido-detrompeta.mp3", "imagen_14.png"]);
records.set("AR", ["#c93a3a", "sabes al mes de septiembre...", "slurp.mp3", "imagen_9.jpg"]);
records.set("SA", ["#ff0000", "eres una dulce melodía", "cajamusical.mp3", "algodondeazucar.jpeg"]);
records.set("AS", ["#ff0000", "eres el sonido del sol", "borbujas-2.mp3", "imagen_5.jpg"]);
records.set("TA", ["#b7355d", "delicioso sabor a ruibarbo", "sonido-de-horno.mp3", "textura-3.jpg"]);
records.set("AT", ["#633f63", "salado como la María", "u.mp3", "maria.jpg"]);
records.set("NA", ["#c26363", "das la sensación de ser una nota grave", "DO-MAYOR.wav", "grave.png"]);
records.set("AN", ["#bc5c22", "hueles a los abrazos de tu abuela...cálidos", "viento-arboles.mp3", "arboles.png"]);
records.set("AV", ["#c30606", "tienes sabor a la letra A.", "a.mp3", "textura-4.png"]);
records.set("VA", ["#de0000", "hueles a navidad", "Campanas-de-trineo.mp3", "navidad.jpg"]);
records.set("AW", ["#f79baa", "fresco y calido como el oliva", "chasquido.mp3", "imagen_12.jpg"]);
records.set("WA", ["#f1dce7", "eres como escuchar un algodón de azúcar", "risas-ninas.mp3", "algodondeazucar.jpeg"]);
records.set("XA", ["#eb9898", "eres como la roja brisa del mar", "profundo.mp3", "cielo.jpg"]);
records.set("AX", ["#bb9898", "das la sensación de tener la mirada dulce", "cajamusical.mp3", "cielo.jpg"]);
records.set("YA", ["#ffb000", "me das el sabor de un sándwich con queso derretido", "squich.mp3", "queso.jpg"]);
records.set("AY", ["#eab100", "tu voz suena de forma circular", "borbujas.mp3", "queso.jpg"]);
records.set("ZA", ["#c987a1", "tu voz suena de forma triangular", "patito-de-goma.mp3", "oblicuas.jpg"]);
records.set("AZ", ["#a687a1", "tu voz suena de forma cuadrada", "sonido-debajo-cuadrado.mp3", "textura-3.jpg"]);
records.set("EB", ["#a3dd00", "me suenas a la nota  Fa...", "FA.wav", "textura-notas.png"]);
records.set("BE", ["#adf000", "sabes a...manzana cortada", "crash.mp3", "textura.png"]);
records.set("EC", ["#fadc25", "hueles a piña", "efecto-exprimir.mp3", "pina.jpg"]);
records.set("CE", ["#fddc00", "chirriante como el color amarillo", "puerta.mp3", "brillante.jpg"]);
records.set("DE", ["#7db2b4", "me recuerdas a un recreo con sabor a tutti frutti", "parque.mp3", "algodondeazucar.jpeg"]);
records.set("ED", ["#9c9fb4", "tienes sabor a la letra D", "d.mp3", "textura-d.png"]);
records.set("EF", ["#a1ff8f", "me sabes a... insectos", "squich.mp3", "imagen_12.png"]);
records.set("FE", ["#b5ffbc", "me sabes a frutas tropicales", "sonido-1.mp3", "melon.jpg"]);
records.set("GE", ["#7773b5", "hueles a... cítricos", "flauta.mp3", "textura.png"]);
records.set("EG", ["#73b4bc", "hueles a... especias", "respiracion.mp3", "especias.jpeg"]);
records.set("EH", ["#b4c158", "hueles a piña con especias", "naturaleza-dos.mp3", "pina.jpg"]);
records.set("HE", ["#d9c146", "das  la sensación de un salado acuoso", "u.mp3", "mar.jpeg"]);
records.set("JE", ["#b2ff8b", "eres como una rima de color", "cajamusical.mp3", "color.jpeg"]);
records.set("EJ", ["#b4d681", "tu nombre sabe a mostaza con maní dulce", "flauta.mp3", "yogur.jpeg"]);
records.set("KE", ["#7ad370", "hueles a cilantro", "riego-automatico-cesped.mp3", "tierra.jpeg"]);
records.set("EK", ["#36b52a", "me sabes muy.. verde y con sabor a plantas.", "hojas-secas.mp3", "tierra.jpeg"]);
records.set("EL", ["#b4c519", "hueles a oregano fresco", "hojas-secas.mp3", "tierra.jpeg"]);
records.set("LE", ["#cbf400", "ácido como el color verde de la lima", "chirrido_agudo.mp3", "sonidoagudo.png"]);
records.set("ME", ["#9bcb8d", "tu nombre huele a un perfume amargo", "silbido-tetera.mp3", "textura-negra.jpg"]);
records.set("EM", ["#81cb95", "tienes sabor a la letra E.", "e.mp3", "imagen_9.jpg"]);
records.set("EÑ", ["#69cc6d", "tienes el sabor de la vida... como el olor de la grama", "riego-automatico-cesped.mp3", "pradera.jpg"]);
records.set("ÑE", ["#698f62", "sabes a espárragos", "aclarar-garganta.mp3", "piso.jpg"]);
records.set("PE", ["#50d2a1", "astringente como la lima", "sonido-de-lija.mp3", "sonidoagudo.png"]);
records.set("EP", ["#19d26d", "me das la sensación de tener una textura rugosa", "aspero.mp3", "limon.jpeg"]);
records.set("EQ", ["#889293", "me das la sensación de tener una textura blanda", "u.mp3", "blando.jpeg"]);
records.set("RE", ["#6b6b09", "tienes una sonata cálida", "playa.mp3", "imagen_1.jpg"]);
records.set("ER", ["#aa6b09", "sabes a canela", "ruido-detrompeta.mp3", "especias.jpeg"]);
records.set("SE", ["#c6a759", "tienes sabor al color del ocre", "slurp.mp3", "textura.png"]);
records.set("ES", ["#b4814d", "tienes el sabor que hay entre las 2 de la tarde y las 5 de un sábado", "reloj.mp3", "imagen_7.jpg"]);
records.set("TE", ["#007720", "hueles a café tostado", "viento-arboles.mp3", "cafe-2.jpg"]);
records.set("ET", ["#3e9463", "tienes el sabor del...metal", "chapita.mp3", "imagen_5.jpg"]);
records.set("NE", ["#af9979", "tu nombre sabes a...carne molida", "sonido-de-horno.mp3", "textura.png"]);
records.set("EN", ["#b4c0a4", "tienes sabor a menta fresca", "cajamusical.mp3", "imagen_12.jpg"]);
records.set("EV", ["#4b6b0b", "eres la mezcla purpura entre lo dulce y lo azul del salado", "efecto-exprimir.mp3", "imagen_4.jpg"]);
records.set("VE", ["#396e1c", "eres como una U verde", "u.mp3", "textura-4.png"]);
records.set("EW", ["#a8bcab", "tu nombre me huele a zapato mojado", "avispa.mp3", "tierra.jpeg"]);
records.set("WE", ["#adeebb", "tienes sabor a limón", "chapita.mp3", "limon.jpeg"]);
records.set("XE", ["#e8ece9", "tienes la sensación de tener la voz blanca...", "borbujas.mp3", "leche.jpeg"]);
records.set("EX", ["#98ba98", "hueles a miel y canela", "clang.mp3", "miel.jpeg"]);
records.set("YE", ["#b4ce09", "tienes sabor a margarita", "ruido-detrompeta.mp3", "imagen_9.jpg"]);
records.set("EY", ["#c0f14c", "tienes sabor a tutti frutti", "parque.mp3", "textura-texto.png"]);
records.set("ZE", ["#97aca1", "me das la sensación de un zigzag", "sonido-1.mp3", "zigzag.jpg"]);
records.set("EZ", ["#a4c2bf", "tu nombre suena como el zumbido de una avispa", "avispa.mp3", "textura-rugosa.jpeg"]);
records.set("IB", ["#f0ed94", "tienes sabor a chocolate negro", "clang.mp3", "imagen_7.jpg"]);
records.set("BI", ["#dbd895", "eres un ocre rasposo y muy melancólico", "lluvia.mp3", "imagen_13.jpg"]);
records.set("CI", ["#fedb94", "me sabes a chocolate con fambruesas", "dados.mp3", "imagen_9.jpg"]);
records.set("IC", ["#ebd298", "sabes a... arandanos", "a.mp3", "arandanos.jpeg"]);
records.set("DI", ["#dbb1b4", "eres como una acaricia rosa", "cajamusical.mp3", "algodondeazucar.jpeg"]);
records.set("ID", ["#dbb4ea", "tienes sabor a la letra I.", "i.mp3", "limon.jpeg"]);
records.set("IF", ["#daffce", "tienes el sabor de mayo", "parque.mp3", "pradera.jpg"]);
records.set("FI", ["#b4faa0", "sabes a la sonata de 'El viento en la llanura' de Claude Debussy", "Claude-Debussy.mp3", "imagen_2.jpg"]);
records.set("GI", ["#d6b4ea", "sabes a los 'Azulejos' de Isaac Albéniz", "Isaac-Albeniz.mp3", "imagen_7.jpg"]);
records.set("IG", ["#faf3ea", "das la sensación de la sonata de 'Études I' - György Ligeti", "Gyorgy-Ligeti.mp3", "textura-6.png"]);
records.set("IH", ["#ffc1a4", "me sabes a mermelada de guayaba", "efecto-exprimir.mp3", "hoja-2.jpg"]);
records.set("HI", ["#ffb494", "sabes a frutos secos", "hojas-secas.mp3", "tierra.jpeg"]);
records.set("JI", ["#feffc4", "sabes a frutas frescas", "viento-arboles.mp3", "arboles.png"]);
records.set("IJ", ["#f0f1bf", "sabes a citricos con miel", "flauta.mp3", "miel.jpeg"]);
records.set("KI", ["#d9ffb8", "tienes un sabor de franqueza", "respiracion.mp3", "cielo.jpg"]);
records.set("IK", ["#b7ff94", "tienes la finura y el aroma del canto", "flauta.mp3", "cielo.jpg"]);
records.set("IL", ["#fff494", "sabes a melado de caña", "caballo.mp3", "textura-rugosa.jpeg"]);
records.set("LI", ["#ece694", "me sabes a las calles de sabana grande con olor a pizza", "efecto-deradio.mp3", "caucho.jpg"]);
records.set("MI", ["#dbcacd", "hueles a la bahía de cata", "playa.mp3", "bahiadecata.jpeg"]);
records.set("IM", ["#c8b7dd", "hueles a la suave lavanda ", "brisa.mp3", "textura-2.jpeg"]);
records.set("IÑ", ["#d1cbb2", "aspero y muy pesado", "aspero.mp3", "textura-rugosa.jpeg"]);
records.set("ÑI", ["#bfbb9e", "hueles a perfume dulce", "spray.mp3", "textura-2.jpeg"]);
records.set("PI", ["#c5ffdb", "hueles a libro....o a viruta de lápiz", "pasarpagina.mp3", "libro.jpg"]);
records.set("IP", ["#e1ffe3", "- una paleta de limón", "peces-mar.mp3", "limon.jpeg"]);
records.set("IQ", ["#e2cdd1", "hueles a hogar...a tu pueblo.", "respiracion.mp3", "tierra.jpeg"]);
records.set("RI", ["#d2b494", "hueles a la salvaje pólvora del caricuao", "chapita.mp3", "caucho.jpg"]);
records.set("IR", ["#f8b494", "negra como la noche, sabes a chocolate y albaricoque", "noche.mp3", "imagen_11.jpg"]);
records.set("SI", ["#ef9c81", "hueles a... guardado, como un libro viejo.", "pasarpagina.mp3", "imagen_13.jpg"]);
records.set("IS", ["#d8b895", "hueles a caldo de papas", "lluvia.mp3", "agua.jpeg"]);
records.set("TI", ["#b4bb98", "como el mar..portas el aroma del bacalao", "peces-mar.mp3", "imagen_4.jpg"]);
records.set("IT", ["#b4f496", "hueles a maquillaje", "silbido-pajaro.mp3", "especias.jpeg"]);
records.set("NI", ["#fbd2bf", "picante y fuerte como un tabasco", "hojas-secas.mp3", "picante.jpg"]);
records.set("IN", ["#ffbfd1", "sabes a una bocanada de aire fresco", "respiracion.mp3", "cielo-2.jpg"]);
records.set("IV", ["#c3b495", "cálido como una mañana", "pajaro_mirlo.mp3", "imagen_1.jpg"]);
records.set("VI", ["#e8b596", "hueles a día soleado", "brisa.mp3", "calido.jpg"]);
records.set("IW", ["#f7ede2", "hueles a lluvia", "lluvia.mp3", "imagen_10.jpg"]);
records.set("WI", ["#ebd5d8", "eres una cebolla acuosa", "aclarar-garganta.mp3", "leche.jpeg"]);
records.set("XI", ["#ecebd5", "- crocrante y salado como las papas fritas", "papasfritas.mp3", "papas.jpeg"]);
records.set("IX", ["#b4b495", "tienes olores muy terrosos", "hojas-secas.mp3", "tierra.jpeg"]);
records.set("YI", ["#fffc94", "me sabes a miel", "flauta.mp3", "miel.jpeg"]);
records.set("IY", ["#d7d695", "te siento fresco, como una menta", "flauta.mp3", "cielo.jpg"]);
records.set("ZI", ["#ebe1db", "tu nombre me sabe a cloro y es muy aguado", "cueva.mp3", "agua.jpeg"]);
records.set("IZ", ["#d4b6d9", "eres el canto de las gaviotas", "gaviotas.mp3", "imagen_9.jpg"]);
records.set("OB", ["#9e9eb4", "tu nombre sabe a leche cortada", "tapa.mp3", "leche.jpeg"]);
records.set("BO", ["#696cb8", "eres el sabor de la curiosidad", "chasquido.mp3", "imagen_3.png"]);
records.set("CO", ["#b283b4", "tu nombre es nuy cremoso y huele a shampoo", "slurp.mp3", "yogur.jpeg"]);
records.set("OC", ["#5b50b6", "hueles a organza color lavanda", "olas.mp3", "textura-2.jpg"]);
records.set("DO", ["#7d25ff", "vibras entre lo dulce del color rojo y lo frio del azul", "ruido-detrompeta.mp3", "cielo.jpg"]);
records.set("OD", ["#5c26dc", "tu nombre me huele a crema de manos", "respiracion.mp3", "crema.jpeg"]);
records.set("OF", ["#7ab8e6", "eres como una brisa livana de una tarde color azul", "profundo.mp3", "cielo.jpg"]);
records.set("FO", ["#32b8cc", "azul que huele ligeramente a...Vick's Vaporub", "clang.mp3", "agua.jpeg"]);
records.set("GO", ["#7325ff", "sabes a cerveza artesanal... borbujeas de color violeta", "borbujas.mp3", "agua.jpeg"]);
records.set("OG", ["#926afb", "tienes sabor a la letra O.", "borbujas.mp3", "jabon.jpeg"]);
records.set("OH", ["#b44fc1", "sabes a... queso Vieux Boulogne", "SOL-MAYOR.wav", "queso.jpg"]);
records.set("HO", ["#b32db6", "tienes olor a ropa recien planchada", "aspero.mp3", "textura-2.jpg"]);
records.set("JO", ["#b2b8de", "tienes el aroma de diciembre", "reloj.mp3", "imagen_6.jpg"]);
records.set("OJ", ["#b4b8ff", "eres la sensación del mes de noviembre", "profundo.mp3", "imagen_6.jpg"]);
records.set("KO", ["#7ab8d3", "tu nombre me da la sensación de...un vaso de agua con ajo...", "slurp.mp3", "imagen_3.png"]);
records.set("OK", ["#3e93b9", "tu nombre me da la sensación de... algo muy aspero...", "aspero.mp3", "textura-rugosa.jpg"]);
records.set("OL", ["#b4a9b4", "tu nombre me da la sensación de ser... muy vibrante...", "vibracion.mp3", "sonidoagudo.png"]);
records.set("LO", ["#b4b5e7", "tu nombre me da la sensación de... tener forma rectangular", "sonido-debajo-cuadrado", "textura-3.jpg"]);
records.set("MO", ["#7c64e5", "tu nombre me da la sensación de... tener forma circular", "borbujas.mp3", "queso.jpg"]);
records.set("OM", ["#563dd1", "tu nombre me da la sensación de... tener formar oblicua", "slurp.mp3", "oblicuas.jpg"]);
records.set("OÑ", ["#3f41bd", "eres como el sonido azul de una trompeta", "trompeta.mp3", "sonidoagudo.png"]);
records.set("ÑO", ["#1c29b5", "me suenas a la nota  Sol...", "SOL.wav", "textura-notas.png"]);
records.set("PO", ["#50b8f1", "tu nombre huele a jabón azul", "SOL-MAYOR.wav", "jabon.jpeg"]);
records.set("OP", ["#1ea9e3", "hueles a cobijas recien lavadas", "lavadora.mp3", "textura-2.jpg"]);
records.set("OQ", ["#886ae8", "das la sensación de ser una nota aguda", "SI-MAYOR.wav", "imagen_3.png"]);
records.set("RO", ["#6b25b4", "tienes sabor a un Si mayor", "SI-MAYOR.wav", "imagen_15.png"]);
records.set("OR", ["#b326b5", "me sabes a glaseado de donas", "sonido-de-horno.mp3", "donas.jpg"]);
records.set("SO", ["#ff3cff", "tienes una sonata muy aguda", "chirrido_agudo.mp3", "imagen_5.jpg"]);
records.set("OS", ["#562fb7", "chirriante como el sonido de un auto nuevo", "chirrido.mp3", "sonidoagudo.png"]);
records.set("TO", ["#003fb7", "fría, como las pinceladas de Van Gogh", "flauta.mp3", "cuadro-1.jpeg"]);
records.set("OT", ["#1766c0", "hueles al misterio que se esconde en un bosque", "noche.mp3", "imagen_6.jpg"]);
records.set("NO", ["#af74d9", "tienes un sabor frío y agudo", "SI-MAYOR.wav", "imagen_5.jpg"]);
records.set("ON", ["#6747c1", "eres como un humo fragante", "naturaleza.mp3", "imagen_10.jpg"]);
records.set("OV", ["#4b25b4", "sabes a armonía", "profundo.mp3", "imagen_7.jpg"]);
records.set("VO", ["#8126b5", "sabes a caos, como el de una cola negra", "crash.mp3", "imagen_3"]);
records.set("OW", ["#a89ff8", "tienes el sabor de una rima consonante", "Timbre.mp3", "imagen_15.jpg"]);
records.set("WO", ["#8a7ce5", "tu nombre tiene un sonido... salado...", "sonido-salado.mp3", "imagen_4.jpg"]);
records.set("XO", ["#989cec", "hueles a coco como unas vacaciones en agosto", "olas.mp3", "imagen_4.jpg"]);
records.set("OX", ["#5d5fc9", "tu nombre me huele a una mezcla de colores", "lavadora.mp3", "color.jpg"]);
records.set("YO", ["#b4b4b4", "tu nombre huele a frambuesa", "efecto-exprimir.mp3", "pradera.jpg"]);
records.set("OY", ["#b4a1b4", "tu nombre huele a madera", "crash.mp3", "madera.jpg"]);
records.set("ZO", ["#978cf1", "tu nombre huele a cobre", "Timbre.mp3", "cobre.png"]);
records.set("OZ", ["#8666f7", "eres el sonido purpura que hacen las gaviotas", "gaviotas.mp3", "imagen_9.jpg"]);
records.set("UB", ["#afc338", "me sabes a algodon de azúcar", "risas-ninas.mp3", "algodondeazucar.jpeg"]);
records.set("BU", ["#bcfb00", "hueles a pino", "viento-arboles.mp3", "imagen_6.jpg"]);
records.set("CU", ["#c1ad38", "oh no... tu nombre huele a noni", "aclarar-garganta.mp3", "noni.png"]);
records.set("UC", ["#9a9d3f", "eres como una dulce tarde de tres leches", "cuchara.mp3", "crema-2.jpg"]);
records.set("DU", ["#9277bd", "tu aroma es particular, no la logro descifrar", "profundo.mp3", "imagen_7.jpg"]);
records.set("UD", ["#b343b4", "me recuerdas a un paseo con aroma a café", "viento-arboles.mp3", "cafe.jpg"]);
records.set("UF", ["#783f04", "hueles a palomitas de maíz", "cotufas.mp3", "cotufas.jpeg"]);
records.set("FU", ["#783f04", "hueles a sangría con tequila", "caballo.mp3", "textura-negra.jpg"]);
records.set("GU", ["#93c47d", "sabes a expresionismo puro", "slurp.mp3", "cuadro-3.png"]);
records.set("UG", ["#d9ead3", "sabes al crudo realismo de la contemporaneidad", "slurp.mp3", "cristobal.jpeg"]);
records.set("UH", ["#f3f3f3", "eres la blancura de la letra H y el silencio de la noche", "noche.mp3", "imagen_10.jpg"]);
records.set("HU", ["#b7b7b7", "tu nombre huele a bebé", "estornudo-bebe.mp3", "cielo.jpg"]);
records.set("JU", ["#bf9000", "sabes a bizcocho bañado en chocolate y café", "silbido-tetera.mp3", "bizcocho-cafe.jpg"]);
records.set("UJ", ["#ffe599", "eres un chicle de menta", "brisa.mp3", "blando.jpeg"]);
records.set("KU", ["#a61c00", "eres un bizcochuelo", "slurp.mp3", "madera.jpg"]);
records.set("UK", ["#e6b8af", "me haces pensar en el sabor de un objeto, pero aún no hayo qué", "reloj.mp3", "cielo.jpg"]);
records.set("UL", ["#f9cb9c", "tienes sabor a un anillo de plástico de color", "patito-de-goma.mp3", "crema.jpg"]);
records.set("LU", ["#fce5cd", "eres la sensación de tocar un párpado", "Timbre.mp3", "textura.png"]);
records.set("MU", ["#990000", "eres una galleta de mantequilla", "sonido-de-horno.mp3", "mantequilla.jpeg"]);
records.set("UM", ["#660000", "marrón que huele ligeramente a...", "slurp.mp3", "tierra.jpeg"]);
records.set("UÑ", ["#f1c232", "eres como el sabor del encierro y la alarma", "Timbre.mp3", "imagen_3.png"]);
records.set("ÑU", ["#7f6000", "me sabes a detergente", "aclarar-garganta.mp3", "jabon.jpeg"]);
records.set("PU", ["#4c1130", "sabes a barniz de uñas", "aclarar-garganta.mp3", "imagen_15.png"]);
records.set("UP", ["#741b47", "de sal y vinagre que han perdido su sabor", "Timbre.mp3", "limon.jpeg"]);
records.set("UQ", ["#d5a6bd", "- salchicha fría", "aclarar-garganta.mp3", "queso.jpg"]);
records.set("QU", ["#ead1dc", "es un chocolate Savoy", "sonido-de-horno.mp3", "maria.jpg"]);
records.set("QUA", ["#f4cccc", "es un chocolate Hershey's", "sonido-de-horno.mp3", "maria.jpg"]);
records.set("QUE", ["#d9ead3", "tu nombre sabe a mucho orden...se siente muy limpio", "silbido-pajaro.mp3", "mar.jpg"]);
records.set("QUI", ["#fff2cc", "sabes a arroz con leche", "slurp.mp3", "leche.jpeg"]);
records.set("QUO", ["#c9daf8", "me das la sensación de... mar", "playa.mp3", "mar.jpg"]);
records.set("RU", ["#ff9900", "sabes a caramelo suave", "borbujas-2.mp3", "miel.jpeg"]);
records.set("UR", ["#783f04", "hueles a la llegada del otoño", "hojas-secas.mp3", "imagen_2.jpg"]);
records.set("SU", ["#ff0000", "me suenas a la nota  Do... ", "DO.wav", "textura-notas.png"]);
records.set("US", ["#990000", "gelatinoso con sabor a frambuesa", "slurp.mp3", "agua.jpeg"]);
records.set("TU", ["#0000ff", "tienes sabor a un Sol mayor", "SOL-MAYOR.wav", "textura-notas.png"]);
records.set("UT", ["#134f5c", "tu nombre sabe a un verso", "ruido-detrompeta.mp3", "imagen_9.jpg"]);
records.set("NU", ["#674ea7", "gomitas derretidas", "sonido-de-horno.mp3", "miel.jpeg"]);
records.set("UN", ["#351c75", "sabes a... caraotas con azúcar", "tapa.mp3", "miel.jpeg"]);
records.set("UV", ["#00ffff", "tienes sabor a un La mayor", "LA-MAYOR.wav", "textura-notas.png"]);
records.set("VU", ["#c9daf8", "tu nombre  huele a cabello grasoso...", "aspero.mp3", "tierra.jpeg"]);
records.set("UW", ["#6aa84f", "tu nombre sabe a jugo de naranja", "silbido-pajaro.mp3", "limon.jpeg"]);
records.set("WU", ["#45818e", "sabe a atún", "peces.mp3", "bahiadecata.jpeg"]);
records.set("XU", ["#aaffff", "tu nombre huele a ropa nueva", "aspero.mp3", "textura-2.jpg"]);
records.set("UX", ["#1155cc", "tu nombre sabe a.... malta", "slurp.mp3", "agua.jpeg"]);
records.set("YU", ["#cc0000", "tu nombre sabe a...cola negra", "lata-de-refresco.mp3", "agua.jpeg"]);
records.set("UY", ["#e69138", "tu nombre me sabe a pasas con almendras y yogur", "slurp.mp3", "yogur.jpeg"]);
records.set("ZU", ["#ff9900", "tu nombre tiene sabor a las pinceledas de Matisse", "silbido-pajaro.mp3", "cuadro-2.jpg"]);
records.set("UZ", ["#783f04", "sabe a barra de chocolate rancia", "aspero.mp3", "tierra.jpeg"]);
records.set("AE", ["#dd7f00", "sabes a...queso cheddar", "papasfritas.mp3", "yogur.jpeg"]);
records.set("AI", ["#ffb494", "me sabes a doritos", "crujido.mp3", "especias.jpeg"]);
records.set("AO", ["#b425b4", "sabes como a crema chantilly con gomitas de colores", "sonido-de-horno.mp3", "textura-7.jpg"]);
records.set("AU", ["#FFFF00", "me suenas a la nota  Mi...", "MI.wav", "textura-notas.png"]);
records.set("EA", ["#00ffff", "me suenas a la nota La...", "LA.wav", "textura-notas.png"]);
records.set("EI", ["#FFFF00", "eres amarillo como el mes de febrero", "Timbre.mp3", "textura-6.png"]);
records.set("EO", ["#00ffff", "tienes un sabor azul", "SOL.wav", "imagen_9.jpg"]);
records.set("EU", ["#FF7700", "tienes sabor a un Re mayor", "RE-MAYOR.wav", "textura-notas.png"]);
records.set("IA", ["#FF0000", "tienes sabor a Do mayor", "DO-MAYOR.wav", "textura-notas.png"]);
records.set("IE", ["#FFFF00", "tienes sabor a un Mi mayor", "MI-MAYOR.wav", "textura-notas.png"]);
records.set("IO", ["#77FF00", "tienes sabor a un Fa mayor", "FA-MAYOR.wav", "textura-notas.png"]);
records.set("IU", ["#FFFF00", "eres como un chasquido amarillo", "chasquido.mp3", "limon.jpeg"]);
records.set("OA", ["#FF7700", "eres como una brisa cálida, color naranja", "Timbre.mp3", "imagen_1.jpg"]);
records.set("OE", ["#FFFF00", "sabes al color amarillo", "cajamusical.mp3", "limon.jpeg"]);
records.set("OI", ["#FF7700", "eres el sonido de un colibrí en una mañana color naranja", "colibri.mp3", "imagen_1.jpg"]);
records.set("OU", ["#FF0000", "eres el sonido rojo vibrante de la bateria", "bateria.mp3", "oblicuas.jpg"]);
records.set("UA", ["#4a86e8", "eres como una O azul", "borbujas.mp3", "jabon.jpeg"]);
records.set("UE", ["#FFFF00", "eres como una I amarilla", "flauta.mp3", "oblicuas.jpg"]);
records.set("UI", ["#FF0000", "condimentado, aromatico y rojo", "dados.mp3", "especias.jpeg"]);
records.set("UO", ["#cc4125", "picante y chirriante", "chirrido_agudo.mp3", "picante.jpg"]);


  